<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://i.imgur.com/6wj0hh6.jpg" alt="Project logo"></a>
</p>

<h3 align="center">PROVEE - PROgressiVe Explainable Embeddings</h3>

<div align="center">


[![Status](https://img.shields.io/badge/status-active-success.svg)]()

[![GitHub Issues](https://img.shields.io/github/issues/kylelobo/The-Documentation-Compendium.svg)](https://github.com/kylelobo/The-Documentation-Compendium/issues)
![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/vig/provee/master?gitlab_url=https%3A%2F%2Fgit.science.uu.nl)
![Gitlab pipeline status](https://git.science.uu.nl/vig/provee/dummy-vue-grpc/badges/master/pipeline.svg)
![Gitlab coverage](https://git.science.uu.nl/vig/provee/dummy-vue-grpc/badges/master/coverage.svg)

[![GitHub Issues](https://img.shields.io/github/issues/kylelobo/The-Documentation-Compendium.svg)](https://github.com/kylelobo/The-Documentation-Compendium/issues)
[![GitHub Pull Requests](https://img.shields.io/github/issues-pr/kylelobo/The-Documentation-Compendium.svg)](https://github.com/kylelobo/The-Documentation-Compendium/pulls)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](/LICENSE)

</div>

---

<p align="center"> 

Deep Neural Networks (DNNs), and their resulting **latent or embedding data spaces, are key to analyzing big data** in various domains such as vision, speech recognition, and natural language processing (NLP). However, embedding spaces are high-dimensional and abstract, thus not directly understandable. We aim to develop a software framework to visually explore and explain how embeddings relate to the actual data fed to the DNN. This enables both DNN developers and end-users to understand the currently black-box working of DNNs, leading to better-engineered networks, and explainable, transparent DNN systems whose behavior can be trusted by their end-users. 

Our central aim is to open DNN black-boxes, making complex data understandable for data science novices, and raising trust/transparency are core topics in VA and NLP research. PROVEE will advertise and apply VA in a wider scope with impact across sciences (medicine, engineering, biology, physics) where researchers use big data and deep learning.
</p>

## 📝 Table of Contents

- [Deployment Guide](#guide)
- [Hardware](#hardware)
- [Software](#software)

## 🧐 Deployment Guide <a name = "guide"></a>

empty for now


## 🧰 Hardware Requirements <a name = "hardware"></a>

empty for now


## 💾 Software Requirements <a name = "software"></a>

empty for now