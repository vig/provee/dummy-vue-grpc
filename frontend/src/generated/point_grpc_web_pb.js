/**
 * @fileoverview gRPC-Web generated client stub for pointPackage
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.pointPackage = require('./point_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.pointPackage.PointClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.pointPackage.PointPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pointPackage.voidNoParam,
 *   !proto.pointPackage.PointItem>}
 */
const methodDescriptor_Point_readPoint = new grpc.web.MethodDescriptor(
  '/pointPackage.Point/readPoint',
  grpc.web.MethodType.UNARY,
  proto.pointPackage.voidNoParam,
  proto.pointPackage.PointItem,
  /**
   * @param {!proto.pointPackage.voidNoParam} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pointPackage.PointItem.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pointPackage.voidNoParam,
 *   !proto.pointPackage.PointItem>}
 */
const methodInfo_Point_readPoint = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pointPackage.PointItem,
  /**
   * @param {!proto.pointPackage.voidNoParam} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pointPackage.PointItem.deserializeBinary
);


/**
 * @param {!proto.pointPackage.voidNoParam} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pointPackage.PointItem)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pointPackage.PointItem>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pointPackage.PointClient.prototype.readPoint =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pointPackage.Point/readPoint',
      request,
      metadata || {},
      methodDescriptor_Point_readPoint,
      callback);
};


/**
 * @param {!proto.pointPackage.voidNoParam} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pointPackage.PointItem>}
 *     Promise that resolves to the response
 */
proto.pointPackage.PointPromiseClient.prototype.readPoint =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pointPackage.Point/readPoint',
      request,
      metadata || {},
      methodDescriptor_Point_readPoint);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pointPackage.voidNoParam,
 *   !proto.pointPackage.PointItem>}
 */
const methodDescriptor_Point_readPointsStream = new grpc.web.MethodDescriptor(
  '/pointPackage.Point/readPointsStream',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.pointPackage.voidNoParam,
  proto.pointPackage.PointItem,
  /**
   * @param {!proto.pointPackage.voidNoParam} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pointPackage.PointItem.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pointPackage.voidNoParam,
 *   !proto.pointPackage.PointItem>}
 */
const methodInfo_Point_readPointsStream = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pointPackage.PointItem,
  /**
   * @param {!proto.pointPackage.voidNoParam} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pointPackage.PointItem.deserializeBinary
);


/**
 * @param {!proto.pointPackage.voidNoParam} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.pointPackage.PointItem>}
 *     The XHR Node Readable Stream
 */
proto.pointPackage.PointClient.prototype.readPointsStream =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/pointPackage.Point/readPointsStream',
      request,
      metadata || {},
      methodDescriptor_Point_readPointsStream);
};


/**
 * @param {!proto.pointPackage.voidNoParam} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.pointPackage.PointItem>}
 *     The XHR Node Readable Stream
 */
proto.pointPackage.PointPromiseClient.prototype.readPointsStream =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/pointPackage.Point/readPointsStream',
      request,
      metadata || {},
      methodDescriptor_Point_readPointsStream);
};


module.exports = proto.pointPackage;

