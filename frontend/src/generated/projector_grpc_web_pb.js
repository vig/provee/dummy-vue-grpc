/**
 * @fileoverview gRPC-Web generated client stub for provee
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js')
const proto = {};
proto.provee = require('./projector_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.provee.ProjectorClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.provee.ProjectorPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_Projector_start = new grpc.web.MethodDescriptor(
  '/provee.Projector/start',
  grpc.web.MethodType.UNARY,
  google_protobuf_empty_pb.Empty,
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Empty,
 *   !proto.google.protobuf.Empty>}
 */
const methodInfo_Projector_start = new grpc.web.AbstractClientBase.MethodInfo(
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.provee.ProjectorClient.prototype.start =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/provee.Projector/start',
      request,
      metadata || {},
      methodDescriptor_Projector_start,
      callback);
};


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     Promise that resolves to the response
 */
proto.provee.ProjectorPromiseClient.prototype.start =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/provee.Projector/start',
      request,
      metadata || {},
      methodDescriptor_Projector_start);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_Projector_stop = new grpc.web.MethodDescriptor(
  '/provee.Projector/stop',
  grpc.web.MethodType.UNARY,
  google_protobuf_empty_pb.Empty,
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Empty,
 *   !proto.google.protobuf.Empty>}
 */
const methodInfo_Projector_stop = new grpc.web.AbstractClientBase.MethodInfo(
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.provee.ProjectorClient.prototype.stop =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/provee.Projector/stop',
      request,
      metadata || {},
      methodDescriptor_Projector_stop,
      callback);
};


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     Promise that resolves to the response
 */
proto.provee.ProjectorPromiseClient.prototype.stop =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/provee.Projector/stop',
      request,
      metadata || {},
      methodDescriptor_Projector_stop);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.provee.TrainingSet,
 *   !proto.provee.Point>}
 */
const methodDescriptor_Projector_getProjectionPoints = new grpc.web.MethodDescriptor(
  '/provee.Projector/getProjectionPoints',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.provee.TrainingSet,
  proto.provee.Point,
  /**
   * @param {!proto.provee.TrainingSet} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.provee.Point.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.provee.TrainingSet,
 *   !proto.provee.Point>}
 */
const methodInfo_Projector_getProjectionPoints = new grpc.web.AbstractClientBase.MethodInfo(
  proto.provee.Point,
  /**
   * @param {!proto.provee.TrainingSet} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.provee.Point.deserializeBinary
);


/**
 * @param {!proto.provee.TrainingSet} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.provee.Point>}
 *     The XHR Node Readable Stream
 */
proto.provee.ProjectorClient.prototype.getProjectionPoints =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/provee.Projector/getProjectionPoints',
      request,
      metadata || {},
      methodDescriptor_Projector_getProjectionPoints);
};


/**
 * @param {!proto.provee.TrainingSet} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.provee.Point>}
 *     The XHR Node Readable Stream
 */
proto.provee.ProjectorPromiseClient.prototype.getProjectionPoints =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/provee.Projector/getProjectionPoints',
      request,
      metadata || {},
      methodDescriptor_Projector_getProjectionPoints);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.provee.Point>}
 */
const methodDescriptor_Projector_getUpdates = new grpc.web.MethodDescriptor(
  '/provee.Projector/getUpdates',
  grpc.web.MethodType.SERVER_STREAMING,
  google_protobuf_empty_pb.Empty,
  proto.provee.Point,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.provee.Point.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Empty,
 *   !proto.provee.Point>}
 */
const methodInfo_Projector_getUpdates = new grpc.web.AbstractClientBase.MethodInfo(
  proto.provee.Point,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.provee.Point.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Empty} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.provee.Point>}
 *     The XHR Node Readable Stream
 */
proto.provee.ProjectorClient.prototype.getUpdates =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/provee.Projector/getUpdates',
      request,
      metadata || {},
      methodDescriptor_Projector_getUpdates);
};


/**
 * @param {!proto.google.protobuf.Empty} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.provee.Point>}
 *     The XHR Node Readable Stream
 */
proto.provee.ProjectorPromiseClient.prototype.getUpdates =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/provee.Projector/getUpdates',
      request,
      metadata || {},
      methodDescriptor_Projector_getUpdates);
};


module.exports = proto.provee;

