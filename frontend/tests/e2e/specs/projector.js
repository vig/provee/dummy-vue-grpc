describe('Projector', () => {
  it('Uploads CSV file and receives points', () => {
    cy.visit('/')

    const testFile = 'data.csv'
    cy.get('[data-test-id="file-input"]').attachFile(testFile)
    cy.get('[data-test-id="submit-button"]').click()

    cy.get('[data-test-id="counter-value"]').should(div => {
      const counter = parseInt(div.text(), 10)
      expect(counter).to.be.greaterThan(0)
    })
  })
})
