describe('Home', () => {
  it('Visits the app root url', () => {
    cy.visit('/')
    cy.contains('.v-toolbar__title', 'PROVee')
  })
})
