
from confluent_kafka import Consumer, KafkaError, KafkaException
import random
rnd = random.uniform(1.5, 10000000)
conf = {'bootstrap.servers': "localhost:9092",
        'group.id': "test"+str(rnd),
        'auto.offset.reset': 'earliest'}

consumer = Consumer(conf)


running = True
count = 0
def basic_consume_loop(consumer, topics):
    global count
    try:
        consumer.subscribe(topics)

        while running:
            msg = consumer.poll(timeout=1.0)
            if msg is None: continue

            if msg.error():
                if msg.error().code() == KafkaError._PARTITION_EOF:
                    # End of partition event
                    sys.stderr.write('%% %s [%d] reached end at offset %d\n' %
                                     (msg.topic(), msg.partition(), msg.offset()))
                elif msg.error():
                    raise KafkaException(msg.error())
            else:
                count += 1
                if count % 1000 == 0:
                    print("Stored", count, "points")
    finally:
        # Close down consumer to commit final offsets.
        consumer.close()
        print("total:" ,count)

def shutdown():
    running = False


basic_consume_loop(consumer,["pointData0"])
