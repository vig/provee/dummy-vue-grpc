from confluent_kafka.admin import AdminClient, NewTopic, NewPartitions, ConfigResource
import confluent_kafka
import time
conf = {'bootstrap.servers': 'localhost:9092'}
adminClient = AdminClient(conf)




def create_topic(userID):
    topicName = "pointData" + str(userID)    
    fs = adminClient.create_topics([NewTopic(topicName, 1, 1)],operation_timeout=30)
    for topic, f in fs.items():
        try:
            f.result()  # The result itself is None
            print("Topic {} created".format(topic))
        except Exception as e:
            print("Failed to create topic {}: {}".format(topic, e))

    print("Created topics")


def deleteTopics(topics):
    fs = adminClient.delete_topics(topics, operation_timeout=30)

    # Wait for operation to finish.
    for topic, f in fs.items():
        try:
            f.result()  # The result itself is None
            print("Topic {} deleted".format(topic))
        except Exception as e:
            print("Failed to delete topic {}: {}".format(topic, e))
            
def deleteUserTopics():
    topics = []
    for i in range(5):
        topicName = "pointData" + str(i)
        print("Deleting",topicName)
        topics.append(topicName)
    deleteTopics(topics)

create_topic(4)
print(adminClient.list_topics().topics)
#deleteUserTopics()
print(adminClient.list_topics().topics)




    
