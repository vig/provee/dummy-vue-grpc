from confluent_kafka.admin import AdminClient, NewTopic, NewPartitions, ConfigResource
from confluent_kafka import Producer
import confluent_kafka
import time
import json
import asyncio
import socket
import websockets

adminConf = {'bootstrap.servers': '127.0.0.1:9092'}
producerConf = conf = {'bootstrap.servers': '127.0.0.1:9092', 'client.id': socket.gethostname()}
adminClient = AdminClient(adminConf)
producer = Producer(conf)


def userIDToTopic(userID):
    return  "pointData" + str(userID)

def create_topic(topicName):
    fs = adminClient.create_topics([NewTopic(topicName, 1, 1)],operation_timeout=30)
    for topic, f in fs.items():
        try:
            f.result()  # The result itself is None
            print("Topic {} created".format(topic))
        except Exception as e:
            print("Failed to create topic {}: {}".format(topic, e))

    print("Created topics")

def deleteUserTopic(userID):
    deleteTopics([userIDToTopic(userID)])


def deleteAllTopics():
    topicDict = adminClient.list_topics().topics
    deleteTopics(list(topicDict.keys()))



def deleteTopics(topics,printB = True):
    if len(topics) == 0:
        return

    topics = [el  for el in topics if el[0] != "_"]  
    fs = adminClient.delete_topics(topics, operation_timeout=30)

    # Wait for operation to finish.
    for topic, f in fs.items():
        try:
            f.result()  # The result itself is None
            print("Topic {} deleted".format(topic))
        except Exception as e:
            if(printB):
                print("Failed to delete topic {}: {}".format(topic, e))
            
def deleteUserTopics():
    topics = []
    for i in range(100):
        topicName = userIDToTopic(i)
        topics.append(topicName)
    deleteTopics(topics,False)


connected = {}

def acked(err, msg):
    if err is not None:
        print("Failed to deliver message: %s: %s" % (str(msg), str(err)))


count = 0
count2 = 0
async def parseMessage(websocket, message):
    global count
    jsonMessage = json.loads(message)
    if jsonMessage["type"] == "GetUserID":
        if websocket  in connected.keys():
            return
        userID = len(connected)
        topicName = userIDToTopic(userID)
        connected[websocket] = userID
        await websocket.send(json.dumps({"type": "userID","userID":userID,"kafkaTopic": topicName}))
        print("Returning",userID)
        create_topic(topicName)
    elif  jsonMessage["type"] == "UploadPoint":
        topicName = jsonMessage["topic"]
        point = jsonMessage["point"]
        producer.produce(topicName, value="__".join(point), callback=acked)
        producer.poll(0)
        count += 1
        if count %1000 == 0:
            print("Processed",count,"points")

    else:
        print("Invalid message",message)


async def server(websocket, path):
    # Register.
    print("Client connected")
    try:
        async for message in websocket:
            await parseMessage(websocket,message)  

    finally:
        # Unregister.
        print("Client disconnected")
        if websocket not in connected.keys():
            return
        deleteUserTopic(connected[websocket])
        del connected[websocket]
    


deleteAllTopics()
print(adminClient.list_topics().topics)
start_server = websockets.serve(server, "127.0.0.1", 1234)
print("Server started")
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()