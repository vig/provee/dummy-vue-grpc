from confluent_kafka import Producer
import socket
import time

cid = socket.gethostname()
print(cid)
conf = {'bootstrap.servers': "localhost:9092",
        'client.id': socket.gethostname()}

producer = Producer(conf)


for i in range(100):
    producer.produce("pointData2", value=str(i).encode("utf-8"))
    print("message sent")
    time.sleep(4)
