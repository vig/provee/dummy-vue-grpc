var PROTO_PATH = __dirname + '/../protos/v3/projector.proto';

var grpc = require('@grpc/grpc-js');
var protoLoader = require('@grpc/proto-loader');
var packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
     longs: String,
     enums: String,
     defaults: true,
     oneofs: true
    });


const port = process.env.GRPC_SERVER_PORT || "50051"
/**
 * Implements the SayHello RPC method.
 */

function main() {
  var provee_proto = grpc.loadPackageDefinition(packageDefinition).provee;
  var server = new grpc.Server();
  server.addService(provee_proto.Projector.service, {getProjectionPoints: getProjectionPoints});
  const url = '0.0.0.0:' + port;
  server.bindAsync(url, grpc.ServerCredentials.createInsecure(), () => {
    server.start();
    console.log("Started server! on " + url);
  });
}
main();

var pointID = 0;
function generatePoint() {
  const id = pointID++;
  const x = Math.floor(Math.random() * 100);
  const y = Math.floor(Math.random() * 100);

  return { id, x, y };
}

//The client calls this
function getProjectionPoints(call){
  console.log("connected");
  call.on('data',(msg)=>{
    if(msg.hdvector.length > 0){
      echoPoint(call,msg.hdvector);
    }
    else{
      sendProjectionPoints(call);
    }

  });
  call.on('end',()=>{
    console.log("end ");
  });
}

//Send the point back
function echoPoint(call, hdvector){
  var response = {id : pointID, x: hdvector[0], y: hdvector[1]};
  call.write(response);
  pointID += 1;

  if(pointID%1000==0){
    console.log("Processed " + pointID + " points")
  }
}

//In case of no points
function sendProjectionPoints(call){
  console.log("Sending generated points");
  var intervalId = setInterval(function(){    
    for(var i = 0; i<100;i++){      
      call.write(generatePoint());
    }
    setTimeout(function() {
      clearInterval(intervalId)
      call.end();

    }, 100000);

},0);
}