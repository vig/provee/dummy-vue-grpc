
import numpy as np
from sklearn.decomposition import IncrementalPCA
import time
import pickle as pk

PATH = "C:\\Users\\simen\\OneDrive\\Documents\\dataFiles\\"
FILE = "word2vec_reddit_300_500000"
ipca = IncrementalPCA(n_components=5, batch_size=16)

TRAINING_BUFFER_SIZE = 10000

def partialFit(buffer):
    ipca.partial_fit(buffer)

def lineToVector(line):
    lineSplit = line.split(" ")

    return [float(elem) for elem in lineSplit[1:]]

def trainPCA():
    with open(PATH+FILE+".txt", encoding="utf8") as f:
        buffer = []
        for line in f:
            buffer.append(lineToVector(line))
            if len(buffer) == TRAINING_BUFFER_SIZE:
                partialFit(buffer)
                buffer = []

        if len(buffer) != 0:
            partialFit(buffer)




def transformPCA():
    with open(PATH + FILE + ".txt", encoding="utf8") as f:
        for line in f:
            ipca.transform([lineToVector(line)])


        print("Explained ratio: ",ipca.explained_variance_ratio_)
        print("Explained total: ", sum(ipca.explained_variance_ratio_))



t0 = time.time()

trainPCA()
print(ipca.get_params(True))
pk.dump(ipca, open("pca2.pkl","wb"))

t1 = time.time()
transformPCA()


print("Training took", t1-t0,"seconds")
t2 = time.time()
print("Transforming took", t2 - t1, "seconds")

