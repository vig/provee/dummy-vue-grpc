# PROVEE

## Development

Run both [backend](backend/README.md) and [frontend](frontend/README.md).

Generate gRPC service classes whenever [protos](protos/README.md) change.
