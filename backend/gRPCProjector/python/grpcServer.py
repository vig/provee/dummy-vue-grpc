from concurrent import futures
import grpc
import projector_pb2_grpc as rpc

import projector_pb2 as projector

class ProjectorService(rpc.ProjectorServicer):

    def __init__(self):
        self.pointID = 0
        
    
    def getProjectionPoints(self, request_iterator, context):
        print("Connected")
        for trainingChunk in request_iterator:
            pointChunkBuffer = []
            for row in trainingChunk.rows:
                pointChunkBuffer.append(self.rowToPoint(row))
                
         
            pointChunk = projector.PointChunk(points=pointChunkBuffer)
            yield pointChunk            
                 

    def rowToPoint(self, row):
        hdvector = row.hdvector
        returnPoint = projector.Point(id=self.pointID, x = float(hdvector[0]), y=float(hdvector[1]))
        self.pointID += 1

        if self.pointID % 1000 == 0:
            print("Received {} points!".format(self.pointID))

        return returnPoint
    
def serveServer():
    port = '[::]:50051'
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    rpc.add_ProjectorServicer_to_server(ProjectorService(), server)
    server.add_insecure_port(port)
    server.start()
    print("Listening on port: " + port)
    server.wait_for_termination()


print("test")
if __name__ == '__main__':
    print("Starting server")
    serveServer()
