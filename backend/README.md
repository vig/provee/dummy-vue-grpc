# provee-backend

## Local development
Run the gateway in `\webSocketGateway` using `npm start`.
Run the projector in `\gRPCProjector\node` or `\gRPCProjector\python` using  `npm start` or `python3 grpcServer.py` respectively.


## Deploy with kubernetes

With minikube you can deploy the deployment.yaml file.
Then run `minikube tunnel` to expose the external IP


## Development

`Dockerfile.dev` defines a development container with `nodemon`. It requires to attach the source code directory as volume for automatic reloading of code changes.

`docker-compose.yml` automatically builds a container with `Dockerfile.dev`.

```
docker-compose up --build --remove-orphans
```

Services can be scaled with `--scale` argument, e.g. `--scale projector=4`.

## Production

`Dockerfile` defines a production container.

There is no `docker-compose.yml` using it yet, because production deployment is likely going to use Kubernetes instead.
