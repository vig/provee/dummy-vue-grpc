
var grpc = require('@grpc/grpc-js');
var protoLoader = require('@grpc/proto-loader');

class GrpcConnection{
    constructor(client){
        this.client = client
        this.calls = null
        this.id = null
    }
}


function rowToTrainingRow(row){
      if (row.length == 0) {
        return null;
      }
  
      var hdvector = [];
      for (var j = 1; j < row.length; j++) {
        hdvector.push(parseFloat(row[j]));
      }
      return { id: row[0], hdvector: hdvector };
}


module.exports = {
    closeClient : function(client){
        grpc.closeClient(client);
    },

    getGRPCPackage : function (PROTO_PATH){
        var packageDefinition = protoLoader.loadSync(
            PROTO_PATH,
            {
            keepCase: true,
            longs: String,
            enums: String,
            defaults: true,
            oneofs: true
            });
    
        return packageDefinition;
    },

    getGRPCClient: function (_target,package,name){
        let pkg = grpc.loadPackageDefinition(package);
        let clientClass = pkg.provee[name];
        client = new clientClass(_target, grpc.credentials.createInsecure());
        let connection = new GrpcConnection(client);
        return connection;
      },   

    getGRPCBidirect: function (ws,call,callback){ 
        console.log(`Connecting  to grpc Server`)
    
        call.on("data", (response) => {
            callback(response, ws)
        });
    
        call.on("error", (err) => {
        console.log(
            `Unexpected stream error: code = ${err.code}` +
            `, message = "${err.message}"`
        );
        console.log("Retrying in 5 seconds");
        setTimeout(function () { getGRPCBidirect(ws,clientCall,callback) }, 5000);
        });
    
        call.on("end", () => { console.log("GRPC connection closed.") })    
        return call;
    },

    //Send row to each microservice
    sendRowToServer : function (allRows,servers) {
    var trainingRows = []

    for (var i = 0; i < allRows.length; i++) {      
        var trainingRow = rowToTrainingRow(allRows[i]);
        if(trainingRow){
            trainingRows.push(trainingRow);
        }
    }

    var trainingChunk = {rows: trainingRows};
    servers.forEach(server => {
        server.calls[0].write(trainingChunk)
    });
  },
    //TODO, extract general setup, and move back to webSocketGateway.js
    getKNNConnection: function (connection){
        connection.knnConn.client.getIDfromServer({},function(error,response){
        if(error){
            console.log("error")
            console.log(error)
        }
        if(!response){            
            return;
        }

        connection.knnConn.id = response["id"];
        const meta = new grpc.Metadata();
        meta.add('id',connection.knnConn.id);

        let call = connection.knnConn.client.sendProjectionPoints(meta,function(error,response){
            console.log("finished points");
            console.log(error);
        });
    
        connection.knnConn.calls=[call]
        console.log("Setup knn call")
        });       
    },    
}







