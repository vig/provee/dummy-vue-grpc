from concurrent import futures
import grpc
import knn_pb2_grpc as rpc
import faiss
import knn_pb2 as knn
import numpy as np
import re
import operator
from tabulate import tabulate

class Data():
    def __init__(self):
        self.allData = []
        self.wordToID = {}
        self.ids = []
        self.index = None

class KNNService(rpc.KNNServicer):
    def __init__(self):
        self.clientList = []
        self.pointID = 0

    #Get client ID
    def metaToID(self,context):
        try:            
            metadict = dict(context.invocation_metadata())
            userid = int(metadict['id'])
            return userid
        except Exception as e:
            print("Error in metaToID (forgot to add user id?)")
            print(e)

    #Store all the data from a new point (can be improved ater)
    def storePoint(self,message, context):
        try:
            dataObject = self.clientList[self.metaToID(context)]
            hdvector = [np.float32(x) for x in message.hdvector]

            if not dataObject.index:
                dataObject.index = faiss.IndexFlatL2(len(hdvector))
            dataObject.ids.append(message.id)
            dataObject.allData.append(hdvector)
            dataObject.wordToID[message.id] = len(dataObject.allData)-1
            dataObject.index.add(np.asarray([hdvector]))
        except Exception as e:
            print(e)

    #Supply client with ID so that it can give it later on with metadata to identify
    def getIDfromServer(self, request,context):
        clientID = len(self.clientList)
        self.clientList.append(Data())
        print("New id: ",clientID)
        return knn.ID(id=str(clientID))
        
    def calculateFinalVector(self,dataObject,request):
        ops = { "+": operator.add, "-": operator.sub }

        strippedWord = "".join(request.words.split())
        wordList = re.split('-|\+',strippedWord)

        wordList = [x.lower() for x in wordList]
        print("Wordlist",wordList)
        for word in wordList:
            if word not in dataObject.wordToID:
                print(word,"not in set yet?")
                return knn.Neighbours()

        orderedOperators = ''.join(c for c in request.words if c in ["-","+"])
        firstID = dataObject.wordToID[wordList[0]]
        resultedVector = np.array(dataObject.allData[firstID])

        for i in range(1,len(wordList)):
            nextID = dataObject.wordToID[wordList[i]]
            resultedVector = ops[orderedOperators[i-1]](resultedVector, np.array(dataObject.allData[nextID]))
        
        return resultedVector
    #Do simple linalg expression with 3 vector, improve later
    def getKNNRequest(self,request,context):
        dataObject = self.clientList[self.metaToID(context)]
        
        resultedVector = self.calculateFinalVector(dataObject,request)
        neighbours = self.knnVector(resultedVector,10,dataObject)
        return neighbours

    #Perform actual KNN
    def knnVector(self,vector,k,dataObject):
        D, I = dataObject.index.search(np.asarray([vector]), k)
        words = [dataObject.ids[x] for x in I[0]]
        distances = D[0]
        returnRows = [knn.Row(id=x,distance=y) for x,y in zip(words,distances)]
        neighbours = knn.Neighbours(rows=returnRows)
        return neighbours

    def sendProjectionPoints(self, request_iterator, context):
        print("Connected")
        try:
            for trainingChunk in request_iterator:
                for row in trainingChunk.rows:
                    self.storePoint(row,context)

                    if self.pointID % 1000 == 0:
                        print("Received {} points!".format(self.pointID))
                    self.pointID+= 1
        except Exception as e:
            print("Error",e)

def serveServer():
    port = '[::]:50052'
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    rpc.add_KNNServicer_to_server(KNNService(), server)
    server.add_insecure_port(port)
    server.start()
    print("Listening on port: " + port)
    server.wait_for_termination()

if __name__ == '__main__':
    print("Starting the KNN server")
    serveServer()
