src.Projections.Python package
==============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   src.Projections.Python.PCA

Submodules
----------

src.Projections.Python.ProjectionTransformer module
---------------------------------------------------

.. automodule:: src.Projections.Python.ProjectionTransformer
   :members:
   :undoc-members:
   :show-inheritance:

src.Projections.Python.Projector module
---------------------------------------

.. automodule:: src.Projections.Python.Projector
   :members:
   :undoc-members:
   :show-inheritance:

src.Projections.Python.Transformer module
-----------------------------------------

.. automodule:: src.Projections.Python.Transformer
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: src.Projections.Python
   :members:
   :undoc-members:
   :show-inheritance:
