src.KNN package
===============

Submodules
----------

src.KNN.faissKNN module
-----------------------

.. automodule:: src.KNN.faissKNN
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: src.KNN
   :members:
   :undoc-members:
   :show-inheritance:
