Source code
===========

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   src.KNN
   src.Projections
   src.UI
   src.utils

Submodules
----------

src.constants module
--------------------

.. automodule:: src.constants
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: src
   :members:
   :undoc-members:
   :show-inheritance:
