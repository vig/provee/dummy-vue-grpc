README
======

.. role:: raw-html-m2r(raw)
   :format: html



.. raw:: html

   <p align="center">
     <a href="" rel="noopener">
    <img width=200px height=200px src="https://i.imgur.com/6wj0hh6.jpg" alt="Project logo"></a>
   </p>



.. raw:: html

   <h3 align="center">PROVEE - PROgressiVe Explainable Embeddings</h3>


----


.. raw:: html

   <p align="center"> 

   Deep Neural Networks (DNNs), and their resulting **latent or embedding data spaces, are key to analyzing big data** in various domains such as vision, speech recognition, and natural language processing (NLP). However, embedding spaces are high-dimensional and abstract, thus not directly understandable. We aim to develop a software framework to visually explore and explain how embeddings relate to the actual data fed to the DNN. This enables both DNN developers and end-users to understand the currently black-box working of DNNs, leading to better-engineered networks, and explainable, transparent DNN systems whose behavior can be trusted by their end-users. 

   Our central aim is to open DNN black-boxes, making complex data understandable for data science novices, and raising trust/transparency are core topics in VA and NLP research. PROVEE will advertise and apply VA in a wider scope with impact across sciences (medicine, engineering, biology, physics) where researchers use big data and deep learning.
   </p>


📝 Table of Contents
--------------------


* `About <#about>`_
* `Getting Started <#getting_started>`_
* `Feature/Performance Comparison <../COMPARISON.md>`_
* `Deployment <#deployment>`_
* `Usage <#usage>`_
* `Built Using <#built_using>`_
* `TODO <../TODO.md>`_
* `Contributing <../CONTRIBUTING.md>`_
* `Authors <#authors>`_
* `Acknowledgments <#acknowledgement>`_

🧐 About :raw-html-m2r:`<a name = "about"></a>`
---------------------------------------------------

In this repository you will find PROVEE, short for Progressive Explainable Embeddings, a visual-interactive system for representing the embedding data spaces in a user-friendly 2D projection. The idea behind `Progressive Analytics <https://arxiv.org/abs/1607.05162>`_\ , such as described e.g. by Fekete and Primet, is to provide a rapid data exploration pipeline with a feedback loop from the system to the analyst with a latency below about 10 seconds. Research has shown that when performing exploratory analysis humans need a latency below about 10 seconds to remain focused and use their short-term memory efficiently. Therefore, PROVEE's goals are (1) to provide increasingly meaningful partial results as the algorithms execute, (2) provide visualizations that minimize distractions by not changing views excessively, (3) will provide cues to indicate where new results have been found by analytics, (4) should provide an interface for users to specify where analytics should focus, as well as the portions of the problem space that should be ignored. *Note that these goals are adapted from the aforementioned publication.*

PROVEE's architecture includes (1) back-end analysis algorithms (particularly, incremental projection algoritms), (2) intuitive, web-based user interfaces/visualizations and (3) intermediate data storage and transfer. Core to our system is an innovative, progressive analysis workflow targeting a human-algorithm feedback-loop with a latency under ~10 seconds to maintain the user's efficiency during exploration tasks. PROVEE will be scalable to big data; generic (handle data from many application domains); and easy to use (requires no specialist programming from the user). Please also refer to our `Performance and feature comparison <../COMPARISON.md>`_ to available (visualization and analysis) tools 

🏁 Getting Started :raw-html-m2r:`<a name = "getting_started"></a>`
-----------------------------------------------------------------------

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See `deployment <#deployment>`_ for notes on how to deploy the project on a live system.

Prerequisites
^^^^^^^^^^^^^


.. raw:: html

   <!-- What things you need to install the software and how to install them. -->



.. code-block::

   Docker, Kubernetes

Installing
^^^^^^^^^^

To install the backend please refer to the `backend README <backend/README.md>`_ and for the frontend to the `frontend README <frontend/README.md>`_.

Generate gRPC service classes whenever `protos <protos/README.md>`_ change.

clone the latest Provee directory from Gitlab

.. code-block::

   git clone https://git.science.uu.nl/vig/provee/dummy-vue-grpc

cd to the cloned provee backend directory

.. code-block::

   cd dummy-vue-grpc/backend

run docker-compose to start all backend services

.. code-block::

   docker-compose up

cd to the cloned provee frontend directory

.. code-block::

   cd dummy-vue-grpc/frondent

ensure Node.js and yarn are installed

build the frontend

.. code-block::

   yarn install
   yarn build


.. raw:: html

   <!-- A step by step series of examples that tell you how to get a development env running.

   Say what the step will be

   ```
   Give the example
   ```

   And repeat

   ```
   until finished
   ``` 
   End with an example of getting some data out of the system or using it for a little demo.
   -->



🔧 Running the tests :raw-html-m2r:`<a name = "tests"></a>`
---------------------------------------------------------------

Explain how to run the automated tests for this system.

Break down into end to end tests
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Explain what these tests test and why

.. code-block::

   Give an example

Basic Unit Tests
^^^^^^^^^^^^^^^^

We use `Jest <https://jestjs.io/>`_ for JavaScript based Unit Tests, 

.. code-block::

   npm run tests

🎈 Usage :raw-html-m2r:`<a name="usage"></a>`
-------------------------------------------------

Notes about how to use the system are TBD, Video coming soon.

🚀 Deployment :raw-html-m2r:`<a name = "deployment"></a>`
-------------------------------------------------------------

If you want to deploy a live system refer to the `Deployment Guide <../DEPLOYMENTGUIDE.md>`_.

⛏️ Built Using :raw-html-m2r:`<a name = "built_using"></a>`
---------------------------------------------------------------


* `VueJs <https://vuejs.org/>`_ - Web Framework
* `NodeJs <https://nodejs.org/en/>`_ - Server Environment
* `PixiJS <https://www.pixijs.com/>`_ - Visualization
* `D3js <https://www.d3js.org/>`_ - Visualization

✍️ Authors :raw-html-m2r:`<a name = "authors"></a>`
-------------------------------------------------------


* `Michael Behrisch <https://michael.behrisch.info>`_ - Idea & Initial work
* `Simen van Herpt <https://github.com/IsolatedSushi>`_ - Backend & Infrastructure
* `Jan Zak <https://zakjan.cz/>`_ - Vis coding

See also the list of `contributors <https://git.science.uu.nl/vig/provee/dummy-vue-grpc/-/graphs/master>`_ who participated in this project.

🎉 Acknowledgements :raw-html-m2r:`<a name = "acknowledgement"></a>`
------------------------------------------------------------------------


* Hat tip to anyone whose code was used
