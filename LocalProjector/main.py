import os
import pickle as pk
import sys
import time
import threading
import pyqtgraph as pg
from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtCore import QThread, QThreadPool, QTimer, QObject
from src.UI.Renderer2D import Renderer2D
from src.UI.Renderer3D import Renderer3D
from src.UI.ui import UI
from src.serviceManager import ServiceManager


class Provee(QObject):
    """Main object for the program, has acces to the UI, and service manager (which handles the connections to the microservices)

    Args:
        QObject (QObject): So that we can use QT signals/slots
    """

    def __init__(self, ui):
        super(Provee, self).__init__()
        self.ui = ui

        self.serviceManager = ServiceManager()
        self.models = []
        self.activeModel = None
        self.activeFilePath = None
        self.startVisualisation()
        self.handleConnections()
        self.ui.show()

    def startVisualisation(self):
        """Creates the visualiser and passes it to the UI
        """
        self.visualiser = Renderer3D(1)
        self.ui.setVisualiser(self.visualiser.getWidget())

    def handleConnections(self):
        """Connects to the button clicked signals, comboBoxes selection changed etc.
        """
        self.ui.uploadFileBTN.clicked.connect(self.uploadFile)
        self.ui.getPrecisionPCA.clicked.connect(self.getPrecision)
        self.ui.getProjectionBTN.clicked.connect(self.setupTransformer)
        self.ui.stopProjectionBTN.clicked.connect(self.stopProjection)
        self.ui.removePointsBTN.clicked.connect(
            self.visualiser.removeAllPoints)
        self.ui.selectModel.currentIndexChanged.connect(self.modelChanged)
        self.ui.loadModelBTN.clicked.connect(self.loadModel)
        self.ui.saveModelBTN.clicked.connect(self.saveModel)
        self.ui.knnRequestBTN.clicked.connect(self.getKNNRequest)
        self.ui.btn_close.clicked.connect(self.closeWindow)
        self.ui.selectDataDirectoryBTN.clicked.connect(self.setDataDirectory)

    def modelChanged(self, value):
        """When a different model is selected in the model selection comboBox

        Args:
            value (int): The index of the selected model
        """
        # That one is empty
        if(value == 0):
            return
        selectedModel = self.models[value - 1]
        self.activeModel = selectedModel

    def saveModel(self):
        """Save the currently selected model
        """
        print("Not implemented yet")
        # self.serviceManager.getProjectorModel()
        # pk.dump(self.PCAProjector.getModel(), open("SavedModels//SavedModel.pkl","wb"))

    def loadModel(self):
        """Load in a model, note that this model must comply with the language used (python with python, cpp with cpp)
        """

        # Select a model
        filePath = QtWidgets.QFileDialog.getOpenFileName(
            self.ui, 'Select File', ".//SavedModels")[0]

        if filePath == "":
            return

        with open(filePath, "rb") as f:
            model = f.read()

        self.models.append(model)

        fileName = filePath.split('/')[-1].split('.')[0]
        self.ui.addModelToList(fileName)

    def getKNNRequest(self):
        """Gets the request for a semantic KNN request, start up a thread which handles that
        """
        knnRequest = self.ui.knnRequest.text()
        if not self.serviceManager.stubs["KNN"]:
            return

        # Since takes long time separate thread
        t = threading.Thread(
            target=self.serviceManager.getKNNRequest, args=(knnRequest,))
        t.start()

    # Start the projection (PCA is only one for now)
    def uploadFile(self):
        """Upload a file for the projection/transforming note that for now
        no checks are being done whether the file is valid
        """

        # Select a file
        filePath = QtWidgets.QFileDialog.getOpenFileName(
            self.ui, 'Select File', os.path.expanduser('~\\Documents\\'))[0]

        if filePath == '':
            return
        self.activeFilePath = filePath
        self.setupKNN(filePath)
        self.setupProjector(filePath)

    def setupKNN(self, filePath):
        """Setup the KNN microservice

        Args:
            filePath (string): The path to the used file so that the KNN can access it aswell
        """

        # Use a separate thread to prevent GUI hanging
        threading.Thread(target=self.serviceManager.setupKNN,
                         args=(filePath,)).start()
        self.serviceManager.KNNprogressSignal.connect(
            self.ui.updateKNNPercentage)

        # Timers cant be started by the new thread, so do it here
        self.serviceManager.knnPercentageTimer.start(1000)

    def setDataDirectory(self):
        """For the setting menu, being able to set a default directory for where the Data files are (WIP)
        """
        print("Getting directory")
        filePath = str(QtWidgets.QFileDialog.getExistingDirectory(
            self.ui, "Select Directory"))
        print(filePath)

    def setupProjector(self, filePath):
        """Setup the Projector microservice (TODO give it the ability to switch between languages)

        Args:
            filePath (string): The path to the used file
        """

        # Use a separate thread to prevent GUI hanging
        threading.Thread(
            target=self.serviceManager.setupProjector, args=(filePath,)).start()
        self.serviceManager.ProjectorprogressSignal.connect(
            self.ui.updateFittingPercentage)
        self.serviceManager.newProjectorModelSignal.connect(self.addModel)

        # Timers cant be started by the new thread, so do it here
        self.serviceManager.projectorPercentageTimer.start(1000)
        self.serviceManager.getModelTimer.start(1000)

    def setupTransformer(self):
        """Setup the Transformer microservice (TODO give it the ability to switch between languages, must be the same as the model used though)
        """

        if not self.activeModel:
            print("No active model")
            return

        # Use a separate thread to prevent GUI hanging
        threading.Thread(target=self.serviceManager.setupTransformer, args=(
            self.activeFilePath, self.activeModel,)).start()
        self.serviceManager.TransformerprogressSignal.connect(
            self.ui.updateTransformPercentage)
        self.serviceManager.newPointsSignal.connect(self.addToVisualiser)

        # Timers cant be started by the new thread, so do it here
        self.serviceManager.transformerPercentageTimer.start(1000)

    def closeWindow(self):
        """Close the UI window
        """
        self.ui.close()

    def addToVisualiser(self, results):
        """Signal from the transformer which has to be forwarded to the UI visualiser

        Args:
            results ([points]): List of points in 2 or 3 dimensions (depending on the projector)
        """
        self.visualiser.addPoints(results)

    def addModel(self, model, modelName=None):
        """Add a model to the model comboBox

        Args:
            model (bytes): The pickle (for python) of the projector (TODO for cpp)
            modelName (string, optional): For in the case of the loadModel, give it the name. Otherwise generate a name for it. Defaults to None.
        """
        self.models.append(model)
        if not modelName:
            modelName = "PCAModel_v" + str(len(self.models))

        self.ui.addModelToList(modelName)

    def getPrecision(self):
        """Python PCA supports a getPrecision function WIP
        """
        print("Not implemented yet")

    # When stop Projection button clicked
    def stopProjection(self):
        """Stop the transformer
        """
        print("Not implemented yet")


def main():
    """Setup the UI and Provee Object
    """
    app = QtWidgets.QApplication(sys.argv)
    ui = UI(Provee)
    ui.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
