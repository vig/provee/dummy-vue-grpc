import pytest
from src.utils.helperFunctions import lineToVector


class TestHelperFunctions:
    def test_lineToVector(self):
        input = 'testword 1.0 1.2 3.4 5.2'
        output = ('testword', [1.0, 1.2, 3.4, 5.2])
        assert lineToVector(input) == output
