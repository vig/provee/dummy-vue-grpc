import grpc
import src.generated.protos.knn_pb2_grpc as knnRpc
import src.generated.protos.knn_pb2 as knn
import src.generated.protos.projector_pb2_grpc as projRpc
import src.generated.protos.projector_pb2 as proj
import src.generated.protos.transformer_pb2 as trans
import src.generated.protos.transformer_pb2_grpc as transRpc
from PyQt5.QtCore import QObject, QTimer, pyqtSignal, QThread
import time
from tabulate import tabulate
import pickle
import copy
import threading
import atexit
import subprocess
import os


class ServiceManager(QObject):
    """Handles the connections to the microservices (KNN,Projector,Transformer)

    Args:
        QObject (QObject): So that we can use signals/slots
    """

    # The signals to which the Provee Object (main) can connect
    KNNprogressSignal = pyqtSignal(int)
    ProjectorprogressSignal = pyqtSignal(int)
    TransformerprogressSignal = pyqtSignal(int)
    newProjectorModelSignal = pyqtSignal(bytes)
    newPointsSignal = pyqtSignal(object)

    def __init__(self):
        super(ServiceManager, self).__init__()

        # Setup the timers to retrieve information from the microservices (start them when connections are being made)
        self.knnPercentageTimer = QTimer()
        self.knnPercentageTimer.timeout.connect(self.getKNNProgress)

        self.projectorPercentageTimer = QTimer()
        self.projectorPercentageTimer.timeout.connect(
            self.getProjectorProgress)

        self.getModelTimer = QTimer()
        self.getModelTimer.timeout.connect(self.getProjectorModel)

        self.transformerPercentageTimer = QTimer()
        self.transformerPercentageTimer.timeout.connect(
            self.getTransformerProgress)

        self.stubs = {"Projector": None, "Transformer": None, "KNN": None}
        self.running = {"Projector": False, "Transformer": False, "KNN": False}

    def setupProjector(self, filePath):
        """Create a separate process for the projector (gRPC)

        Args:
            filePath (string): The path to the file containing the data embedding (which we want to project)
        """

        self.ProjectorProcess = subprocess.Popen(
            ['python', '-m', 'src.Projections.Python.Projector.PCA.PCAProjector'])

        # So that it closes when the main process stops/crashes
        atexit.register(self.ProjectorProcess.terminate)

        # Attempt to connect to the gRPC microservice
        for _ in range(5):
            try:
                channel = grpc.insecure_channel('localhost:50052')
                self.stubs["Projector"] = projRpc.ProjectorStub(channel)
                self.stubs["Projector"].startProjectorService(
                    proj.startProjector(path=filePath))
                self.running["Projector"] = True
                return
            except Exception as e:
                print(e)
                print("Projector not running, retry after 5 seconds")
            time.sleep(5)

        if not self.running["Projector"]:
            print("Stopped trying Projector")

    def setupKNN(self, filePath):
        """Creates a separate process for the KNN (gRPC)

        Args:
            filePath (string): The path to the file containing the data embedding (which we want to search in with KNN)
        """
        self.KNNprocess = subprocess.Popen(
            ['python', '-m', 'src.KNN.faissKNN'])

        # So that it closes when the main process stops/crashes
        atexit.register(self.KNNprocess.terminate)

        # Attempt to connect to the gRPC microservice
        for _ in range(5):
            try:
                print("trying")
                channel = grpc.insecure_channel('localhost:50051')
                self.stubs["KNN"] = knnRpc.KNNStub(channel)
                self.stubs["KNN"].startKNNService(knn.startKNN(path=filePath))
                self.running["KNN"] = True
                return
            except Exception as e:
                print(e)
                print("KNN not running, retrying after 5s")
            time.sleep(5)

        if not self.running["KNN"]:
            print("Stopped trying KNN")

    def setupTransformer(self, filePath, m):
        """Start up the transformer microservice (gRPC)

        Args:
             filePath (string): The path to the file we want to transform (note doesnt have to be the same one as the one which the model was projected on (WIP))
            m (bytes): the pickle result for python of the model
        """
        self.TransProcess = subprocess.Popen(
            ['python', '-m',
             'src.Projections.Python.Transformer.ProjectionTransformer'])

        # So that it closes when the main process stops/crashes
        atexit.register(self.TransProcess.terminate)

        for _ in range(5):
            try:
                channel = grpc.insecure_channel('localhost:50053')
                self.stubs["Transformer"] = transRpc.TransformerStub(channel)

                # Use separate thread since it is a stream of points
                self.t = threading.Thread(
                    target=self.getPoints, args=(filePath, m,))
                self.t.start()
                self.running["Transformer"] = True
                return
            except Exception as e:
                print(e)
                print("Transformer not running, retrying after 5s")
            time.sleep(5)

        if not self.running["Transformer"]:
            print("Stopped trying Transformer")

    def getPoints(self, filePath, m):
        """Slot for the getPoints timer, retrieves the points from the transformer

        Args:
            filePath (string): The path to the file we want to transform (note doesnt have to be the same one as the one which the model was projected on (WIP))
            m (bytes): the pickle result for python of the model
        """

        print("getting points")
        pointBuffer = []

        for pointChunk in self.stubs["Transformer"].startTransformerService(trans.startTransformer(path=filePath, model=m)):
            for point in pointChunk.points:
                point2d = [point.x, point.y]  # TODO handle 3 dimensions aswell
                pointBuffer.append(point2d)

            # Provee conntects to this signal, and forwards the points to the visualiser
            self.newPointsSignal.emit(copy.deepcopy(pointBuffer))
            pointBuffer = []

    def getTransformerProgress(self):
        """Send a gRPC message to transformer to get the progress
        """
        if not self.running["Transformer"]:
            return

        response = self.stubs["Transformer"].getProgress(
            proj.google_dot_protobuf_dot_empty__pb2.Empty())
        self.TransformerprogressSignal.emit(response.progress)
        if response.progress == 100:  # finished
            self.transformerPercentageTimer.stop()

    def getProjectorProgress(self):
        """Send a gRPC message to projector to get the progress
        """
        if not self.running["Projector"]:
            return
        response = self.stubs["Projector"].getProgress(
            proj.google_dot_protobuf_dot_empty__pb2.Empty())
        self.ProjectorprogressSignal.emit(response.progress)
        if response.progress == 100:  # finished
            self.projectorPercentageTimer.stop()

    def getKNNProgress(self):
        """Send a gRPC message to KNN to get the progress
        """
        if not self.running["KNN"]:
            return
        response = self.stubs["KNN"].getProgress(
            knn.google_dot_protobuf_dot_empty__pb2.Empty())
        self.KNNprogressSignal.emit(response.progress)

        if response.progress == 100:  # finished
            self.knnPercentageTimer.stop()

    def getProjectorModel(self):
        """Send a gRPC message to Projector to get the current trained model
        """
        if not self.running["Projector"]:
            return
        response = self.stubs["Projector"].getModel(
            proj.google_dot_protobuf_dot_empty__pb2.Empty())
        self.newProjectorModelSignal.emit(response.model)

        if not self.projectorPercentageTimer.isActive():  # finished
            self.getModelTimer.stop()

    def getKNNRequest(self, sentence):
        """Send a gRPC message to KNN to get the result of KNN request

        Args:
            sentence (string): The linalg expression of the request. For example, king - man + woman
        """
        if not self.running["KNN"]:
            return

        result = self.stubs["KNN"].getKNNRequest(
            knn.knnRequest(k=10, words=sentence))
        if len(result.rows) == 0:  # Something went wrong, check KNN output
            print("error")
            return

        resultList = []
        for row in result.rows:
            resultList.append((row.id, row.distance))

        # Print the tableform of the KNN result
        tableForm = tabulate(resultList, headers=['Word', 'Distance'])
        print(tableForm)
