"""
helperFunctions.py contains utility functions that
could be used anywhere in the project.
"""

from termcolor import colored


def lineToVector(line):
    """
    Parses a line to an object with a vector.

    :param line: The line that needs to be parsed,
     must contain a object followed by a numbers
    :type line: str
    :return: The object and its vector
    :rtype: (str, [float])
    """
    lineSplit = line.split(" ")

    return lineSplit[0], [float(elem) for elem in lineSplit[1:]]


def printColoured(message, service, color):
    """Print the message in the given color (should be different for each microservice)

    Args:
        message (string): The actual message
        service (string): The name of the microservice
        color (string): The name of the color used
    """
    finalMessage = service + "> " + message
    print(colored(finalMessage, color))
