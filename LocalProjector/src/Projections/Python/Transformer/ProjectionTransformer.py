import os
import pickle as pk
import sys
import threading
import time
from concurrent import futures

import grpc
import src.Projections.Python.generated.protos.transformer_pb2 as trans
import src.Projections.Python.generated.protos.transformer_pb2_grpc as rpc
from PyQt5 import QtGui
from PyQt5.QtCore import (QObject, QRunnable, QThread, QTimer, pyqtSignal,
                          pyqtSlot)
from src.constants import *
import src.Projections.Python.Transformer.Transformer as t
from src.utils.helperFunctions import lineToVector, printColoured


class ProjectionTransformer(t.Transformer):
    """The Transformer

    Args:
        t (Transformer): General interface for transformers
    """

    def __init__(self):
        super(ProjectionTransformer, self).__init__()
        self.clearVariables()

    def clearVariables(self):
        """Clear all variables
        """
        self.sizeSoFar = None

    def checkIfAbort(self, count):
        """Check if the transformer should be aborter

        Args:
            count (int): How many points have been processed

        Returns:
            bool: Whether abort or not
        """
        if not self.running:
            printMessage("Transforming aborted")
            return True

        if count >= self.maxCount and self.maxCount > 0:
            printMessage(
                "Finished transforming of {0} points".format(self.maxCount))
            return True

        return False

    def updatePercentage(self):
        """Recalculate the current percentage
        """
        if not self.sizeSoFar:
            return
        self.progress = min([self.sizeSoFar / self.fileSize * 100, 100])

    def getProgress(self, request, context):
        """Implementation for the getProgress gRPC requset

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Returns:
            grPC progressKNN: contains the integer of current progress
        """
        self.updatePercentage()
        return trans.progressTransformer(progress=int(self.progress))

    def transformAndToGRPC(self, buffer):
        """Transform and create the gRPC messages

        Args:
            buffer ([hdvector]): The list of the actual word vectors in

        Returns:
           pointChunk: list of points to be returned to main applications
        """
        results = self.model.transform(buffer)
        pointList = []
        for point in results:
            pointList.append(trans.point(id=1, x=point[0], y=point[1]))
        return trans.pointChunk(points=pointList)

    def startTransformerService(self, request, context):
        """Starts projecting the points

        Args:
            request (gRPC): standard gRPC (contains the messages)
            context (gRPC): standard gRPC

        Yields:
            pointChunk: gRPC messages containing the list of projected points
        """

        # Delete all previous variables (probably from previous datafile training)
        # since we cant hold it in memory. (Possible since we should only have one client)
        self.clearVariables()

        # Transform bytes object to python object
        self.model = pk.loads(request.model)
        self.filePath = request.path
        self.fileSize = os.path.getsize(self.filePath)

        self.maxCount = -1

        count = 0
        self.sizeSoFar = 0
        t1 = time.time()
        with open(self.filePath, "r", encoding="utf8") as f:
            buffer = []
            for line in f:
                # For keeping track how far we are
                self.sizeSoFar += sys.getsizeof(line)
                count += 1

                if self.checkIfAbort(count):
                    return

                # If user wants to specify for example transform 10.000 instead of everything
                _, vector = lineToVector(line)
                buffer.append(vector)
                # If the buffer is full, flush it
                if len(buffer) >= TRANSFORMING_BUFFER_SIZE:
                    pointChunk = self.transformAndToGRPC(buffer)
                    buffer = []
                    yield pointChunk

                buffer.append(vector)
                # If the buffer is full, flush it
                if len(buffer) >= TRANSFORMING_BUFFER_SIZE:
                    pointChunk = self.transformAndToGRPC(buffer)
                    buffer = []
                    yield pointChunk

            # finished
            self.progress = 100
            self.sizeSoFar = self.fileSize

        t2 = time.time()
        printMessage("Finished in {0}".format(t2 - t1))


def printMessage(message):
    """Print the coloured message so that we can see in the output which microservice printed it

    Args:
        message (string): The actual message
    """
    printColoured(message, "Transformer", "yellow")


def serveServer():
    """Setup the GRPC server
    """
    port = 'localhost:50053'
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    rpc.add_TransformerServicer_to_server(ProjectionTransformer(), server)
    server.add_insecure_port(port)
    server.start()
    printMessage("Listening on port: " + port)
    server.wait_for_termination()


if __name__ == '__main__':
    printMessage("Starting the Projector server")
    serveServer()
