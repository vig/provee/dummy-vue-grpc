# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from google.protobuf import empty_pb2 as google_dot_protobuf_dot_empty__pb2
from . import knn_pb2 as protos_dot_knn__pb2


class KNNStub(object):
    """Interface exported by the server.
    """

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.startKNNService = channel.unary_unary(
                '/provee.KNN/startKNNService',
                request_serializer=protos_dot_knn__pb2.startKNN.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )
        self.getKNNRequest = channel.unary_unary(
                '/provee.KNN/getKNNRequest',
                request_serializer=protos_dot_knn__pb2.knnRequest.SerializeToString,
                response_deserializer=protos_dot_knn__pb2.Neighbours.FromString,
                )
        self.getProgress = channel.unary_unary(
                '/provee.KNN/getProgress',
                request_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
                response_deserializer=protos_dot_knn__pb2.progressKNN.FromString,
                )


class KNNServicer(object):
    """Interface exported by the server.
    """

    def startKNNService(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def getKNNRequest(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def getProgress(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_KNNServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'startKNNService': grpc.unary_unary_rpc_method_handler(
                    servicer.startKNNService,
                    request_deserializer=protos_dot_knn__pb2.startKNN.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
            'getKNNRequest': grpc.unary_unary_rpc_method_handler(
                    servicer.getKNNRequest,
                    request_deserializer=protos_dot_knn__pb2.knnRequest.FromString,
                    response_serializer=protos_dot_knn__pb2.Neighbours.SerializeToString,
            ),
            'getProgress': grpc.unary_unary_rpc_method_handler(
                    servicer.getProgress,
                    request_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                    response_serializer=protos_dot_knn__pb2.progressKNN.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'provee.KNN', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class KNN(object):
    """Interface exported by the server.
    """

    @staticmethod
    def startKNNService(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/provee.KNN/startKNNService',
            protos_dot_knn__pb2.startKNN.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def getKNNRequest(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/provee.KNN/getKNNRequest',
            protos_dot_knn__pb2.knnRequest.SerializeToString,
            protos_dot_knn__pb2.Neighbours.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def getProgress(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/provee.KNN/getProgress',
            google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            protos_dot_knn__pb2.progressKNN.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
