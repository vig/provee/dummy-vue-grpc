#!/bin/bash
DIR="$(dirname "$0")"
PROTOS_DIRECTORY="$PWD"
PROJECTOR_PROTO_FILE="$PROTOS_DIRECTORY/projector.proto"
KNN_PROTO_FILE="$PROTOS_DIRECTORY/knn.proto"

BACKEND_PROJECTOR_GENERATED_DIR="$PWD/../backend/gRPCProjector/python"
BACKEND_KNN_GENERATED_DIR="$PWD/../backend/grpcKNN"
BACKEND_GATEWAY_PROTO_DIR="$PWD/../backend/webSocketGateway/protos/v3"
#Projector
python -m grpc_tools.protoc \
        -I "$PROTOS_DIRECTORY" \
        --python_out="$BACKEND_PROJECTOR_GENERATED_DIR" \
        --grpc_python_out="$BACKEND_PROJECTOR_GENERATED_DIR" \
        "$PROJECTOR_PROTO_FILE"

#KNN
python -m grpc_tools.protoc \
        -I "$PROTOS_DIRECTORY" \
        --python_out="$BACKEND_KNN_GENERATED_DIR" \
        --grpc_python_out="$BACKEND_KNN_GENERATED_DIR" \
        "$KNN_PROTO_FILE"

#Copy for the gateway
cp -fr "$PROJECTOR_PROTO_FILE" "$BACKEND_GATEWAY_PROTO_DIR"
cp -fr "$KNN_PROTO_FILE" "$BACKEND_GATEWAY_PROTO_DIR"
echo "Done"