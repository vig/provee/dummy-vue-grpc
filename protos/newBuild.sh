#!/bin/bash
DIR="$(dirname "$0")"
PROTOS_DIRECTORY="$PWD"
KNN_PROTO_FILE="$PROTOS_DIRECTORY/protos/knn.proto"
PROJECTOR_PROTO_FILE="$PROTOS_DIRECTORY/protos/projector.proto"
TRANSFORMER_PROTO_FILE="$PROTOS_DIRECTORY/protos/transformer.proto"

KNN_GENERATED_DIR="$PWD/../LocalProjector/src/KNN/generated"
PROJ_AND_TRANS_GENERATED_DIR="$PWD/../LocalProjector/src/Projections/Python/generated"
SERVICE_GENERATED_DIR="$PWD/../LocalProjector/src/generated"

generate_python_files()
{       
        proto_file=$1
        output_dir=$2
        python -m grpc_tools.protoc \
        -I "$PROTOS_DIRECTORY" \
        --python_out=$output_dir \
        --grpc_python_out=$output_dir \
        $proto_file
}

#PROTO files
generate_python_files "$KNN_PROTO_FILE" "$KNN_GENERATED_DIR"
generate_python_files "$PROJECTOR_PROTO_FILE" "$PROJ_AND_TRANS_GENERATED_DIR"
generate_python_files "$TRANSFORMER_PROTO_FILE" "$PROJ_AND_TRANS_GENERATED_DIR"


#PROTO files for service
generate_python_files "$KNN_PROTO_FILE" "$SERVICE_GENERATED_DIR"
generate_python_files "$PROJECTOR_PROTO_FILE" "$SERVICE_GENERATED_DIR"
generate_python_files "$TRANSFORMER_PROTO_FILE" "$SERVICE_GENERATED_DIR"


echo "Done"