# provee-protos

Contains all gRPC proto files that are used to communicate between services.

## Dependencies

```
brew install protoc
brew install protoc-gen-grpc-node
npm install -g grpc-tools
```

## Development

Generate gRPC service classes whenever protos change.

```
./build.sh
```
